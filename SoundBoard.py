import pygame

def keytochr(key):
    # Converts pygamekey to a uppercase character
    return chr(key - 32)

alphakeys = list(range(97,123)) #Correcponds to the keys A to Z (inclusive)
escapekey = pygame.K_1 #Press 1 to escape the program
allowedkeys = []


#pytgame setup
pygame.init()
display = pygame.display.set_mode((300, 300)) #This is needed to capture events

#Audio setup
pygame.mixer.init(44100,-16,2,1024)
sounds = []
channels = []
print('Loading files')
for key in alphakeys:
    character = keytochr(key)
    filename = f'{character}.mp3'
    try: #Will try to execute this code
        test = open(filename)
    except: #If files doesn't exist, an error is raised and this code is excuted
        print(f'{filename} not found')
    else: #If file does exist, no errors raised so this code is executed
        test.close()
        allowedkeys.append(key)
        sounds.append(0)
        channels.append(0)
        sounds[-1] = pygame.mixer.Sound(filename)
        channels[-1] = pygame.mixer.Channel(len(channels))

print('Soundboard loaded')

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            exit()

        if event.type == pygame.KEYDOWN:
            if event.key == escapekey:
                pygame.quit()
                exit()
            elif event.key in allowedkeys:
                print(f'{keytochr(event.key)} pressed')
                i = allowedkeys.index(event.key)
                channels[i].play(sounds[i])

